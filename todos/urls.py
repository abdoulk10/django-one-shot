from django.urls import path
from todos.views import todo_list, show_todo


urlpatterns = [
    path("<int:id>/", show_todo, name="todo_list_detail"),
    path("", todo_list, name="todo_list_list"),
]
